from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return app.send_static_file('index_page.html')
@app.route("/result")
def result():
    return app.send_static_file('result_page.html')

