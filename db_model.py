import os
from flask import Flask, render_template, session, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config['SECRET_KEY'] = 'hard to guess string'
app.config['SQLALCHEMY_DATABASE_URI'] =\
    'sqlite:///' + os.path.join(basedir, 'data.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


db = SQLAlchemy(app)


class admin(db.Model):
    __tablename__ = 'admin'
    id = db.Column(db.Integer, primary_key=True)
    admin_name = db.Column(db.String(64), unique=True)
    admin_pwd = db.Column(db.String(64))
    

class loginLogs(db.Model):
    __tablename__ = 'loginLogs'
    id = db.Column(db.Integer, primary_key=True)
    admin_name=db.Column(db.String(64))
    login_ip=db.Column(db.String(64))
    login_time = db.Column(db.DateTime,default=datetime.utcnow)

class users(db.Model):
    __tablename__='users'
    id = db.Column(db.Integer,primary_key=True)
    user_ip = db.Column(db.String(64))
    visit_time = db.Column(db.DateTime,default=datetime.utcnow)
    search_content = db.Column(db.String(64))

class crawlHistory(db.Model):
    __tablename__='crawlHistory'
    id = db.Column(db.Integer,primary_key=True)
    target_ip = db.Column(db.String(64))
    start_time = db.Column(db.DateTime,default=datetime.utcnow)
    end_time=db.Column(db.DateTime,default=datetime.utcnow)

class pagesData(db.Model):
    __tablename__='pagesData'
    id = db.Column(db.Integer,primary_key=True)
    page_url= db.Column(db.String(64))
    page_ip = db.Column(db.String(64))
    page_title = db.Column(db.String(64))
    post_time = db.Column(db.DateTime)
    page_html = db.Column(db.String(128))
    page_content = db.Column(db.Text)


